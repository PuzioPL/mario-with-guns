#include "stdafx.h"
#include "Bullet.h"
#include "TextureHolder.h"

Bullet::Bullet()
{
	m_Sprite = Sprite(TextureHolder::GetTexture("graphics/bullet.png"));
}

void Bullet::shoot(Vector2f characterPosition, float targetX, float targetY, Vector2f mousePosition)
{
	m_isInFlight = true;
	m_Position.x = characterPosition.x;
	m_Position.y = characterPosition.y;

	//obroc pocisk
	float angle = (atan2(mousePosition.y - characterPosition.y, mousePosition.x - characterPosition.x) * 180) / 3.141;

	//oblicz gradient drogi pocisku
	float gradient = (characterPosition.x - targetX) / (characterPosition.y - targetY);

	//gradient mniejszy od 1 musi byc ujemny
	if (gradient < 0)
	{
		gradient *= -1;
	}

	//oblicz stosunek pomiedzy x i y
	float ratioXY = m_BulletSpeed / (1 + gradient);

	//ustaw predkosc poziomo i pionowo
	m_BulletDistanceY = ratioXY;
	m_BulletDistanceX = ratioXY * gradient;

	//skieruj pocisk w odpowiednia strone
	if (targetX < characterPosition.x)
	{
		m_BulletDistanceX *= -1;
	}

	if (targetY < characterPosition.y)
	{
		m_BulletDistanceY *= -1;
	}

	//zapisz wyniki do zmiennych
	m_XTarget = targetX;
	m_YTarget = targetY;

	//max zasieg na 2000 pixeli
	float range = 2000;
	m_MinX = characterPosition.x - range;
	m_MaxX = characterPosition.x + range;
	m_MinY = characterPosition.y - range;
	m_MaxY = characterPosition.y + range;

	m_Sprite.setRotation(angle);
	m_Sprite.setPosition(m_Position);
}

void Bullet::stop()
{
	m_isInFlight = false;
}

FloatRect Bullet::getPosition()
{
	return m_Sprite.getGlobalBounds();
}

Sprite Bullet::getSprite()
{
	return m_Sprite;
}

void Bullet::update(float elapsedTime)
{
	m_Position.x += m_BulletDistanceX * elapsedTime;
	m_Position.y += m_BulletDistanceY * elapsedTime;
	
	//porusz pociskiem
	m_Sprite.setPosition(m_Position);

	//czy pocisk osiagnal limity?
	if (m_Position.x < m_MinX || m_Position.x > m_MaxX || m_Position.y < m_MinY || m_Position.y > m_MaxY)
	{
		m_isInFlight = false;
	}
}

bool Bullet::isInFlight()
{
	return m_isInFlight;
}