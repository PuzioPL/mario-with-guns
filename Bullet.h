#pragma once
#include <SFML/Graphics.hpp>

using namespace sf;

class Bullet
{
private:
	Vector2f m_Position;

	Texture m_Texture;

	Sprite m_Sprite;

	bool m_isInFlight;
	float m_BulletSpeed = 2000;

	//ile pikseli pokona pocisk, poziomo i pionowo, w czasie jednej petli
	float m_BulletDistanceX;
	float m_BulletDistanceY;

	//w co pocisk leci
	float m_XTarget;
	float m_YTarget;

	//limity, zeby pocisk nie lecial w nieskonczonosc
	float m_MaxX;
	float m_MinX;
	float m_MaxY;
	float m_MinY;

public:
	Bullet();

	void stop();

	bool isInFlight();

	void Bullet::shoot(Vector2f characterPosition, float targetX, float targetY, Vector2f mousePosition);

	FloatRect getPosition();
	
	Sprite getSprite();

	void update(float elapsedTime);
};