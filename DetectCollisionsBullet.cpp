#include "stdafx.h"
#include "Engine.h"

void Engine::detectCollisionsBullet(Bullet& bullet)
{
	//okresla ktore bloki sprawdza do kolizji
	FloatRect detectionZone = bullet.getPosition();

	//do wykrywania kolizji z otoczeniem
	FloatRect blok;

	blok.width = TILE_SIZE;
	blok.height = TILE_SIZE;

	//strefa w ktorej bedzie sie szukalo kolizji
	int startX = (int)(detectionZone.left / TILE_SIZE) - 1;
	int endX = (int)(detectionZone.left / TILE_SIZE) + 2;

	int startY = (int)(detectionZone.top / TILE_SIZE) - 1;
	int endY = (int)(detectionZone.top / TILE_SIZE) + 2;

	//detection zone nie moze wychodzic poza poziom (inaczej si� skraszuje)
	if (startX < 0) startX = 0;
	if (startY < 0)startY = 0;
	if (endX >= m_LevelManager.getLevelSize().x) endX = m_LevelManager.getLevelSize().x;
	if (endY >= m_LevelManager.getLevelSize().y) endY = m_LevelManager.getLevelSize().y;

	for (int x = startX; x < endX; x++)
	{
		for (int y = startY; y < endY; y++)
		{
			blok.left = x * TILE_SIZE;
			blok.top = y * TILE_SIZE;

			//czy pocisk koliduje z blokiem
			if (m_ArrayLevel[y][x] == 1 || m_ArrayLevel[y][x] == 2 || m_ArrayLevel[y][x] == 3 || m_ArrayLevel[y][x] == 4 || m_ArrayLevel[y][x] == 5 || m_ArrayLevel[y][x] == 6 ||
				m_ArrayLevel[y][x] == 7)
			{
				if (bullet.getPosition().intersects(blok))
				{
					bullet.stop();
				}
			}
		}
	}
}