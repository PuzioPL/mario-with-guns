#include "stdafx.h"
#include "Engine.h"

bool Engine::detectCollisionsChar(Postacie& postac, float dtAsSeconds)
{
	bool koniecPoziomuOsiagniety = false;

	FloatRect detectionZone = postac.getPosition();

	//floatrect pojedynczego bloku aby mozna bylo wykrywac kolizje
	FloatRect blok;

	blok.width = TILE_SIZE;
	blok.height = TILE_SIZE;

	//strefa w ktorej bedzie sie szukalo kolizji
	int startX = (int)(detectionZone.left / TILE_SIZE) - 2;
	int endX = (int)(detectionZone.left / TILE_SIZE) + 3;

	int startY = (int)(detectionZone.top / TILE_SIZE) - 2;
	int endY = (int)(detectionZone.top / TILE_SIZE) + 3;

	//detection zone nie moze wychodzic poza poziom (inaczej si� skraszuje)
	if (startX < 0) startX = 0;
	if (startY < 0)startY = 0;
	if (endX >= m_LevelManager.getLevelSize().x) endX = m_LevelManager.getLevelSize().x;
	if (endY >= m_LevelManager.getLevelSize().y) endY = m_LevelManager.getLevelSize().y;

	//czy postac spadla poza mape
	FloatRect level(0, 0, m_LevelManager.getLevelSize().x * TILE_SIZE, m_LevelManager.getLevelSize().y * TILE_SIZE);

	if (!postac.getPosition().intersects(level))
	{
		//spawn na poczatku mapy
		if (postac.updateHealth(-100, m_GameTimeTotal.asSeconds()))
		{
			m_State = State::GAME_OVER;		
		}
		else
		{
			//postac.spawn(m_LevelManager.getStartPosition());
		}

		m_CurrentScore -= 30;
		if (m_CurrentScore < 0)
		{
			m_CurrentScore = 0;
		}

		//odejmij zycie czy cos
	}
	for (int x = startX; x < endX; x++)
	{
		for (int y = startY; y < endY; y++)
		{
			blok.left = x * TILE_SIZE;
			blok.top = y * TILE_SIZE;

			//czy postac koliduje z blokiem
			if (m_ArrayLevel[y][x] == 1 || m_ArrayLevel[y][x] == 2 || m_ArrayLevel[y][x] == 3 || m_ArrayLevel[y][x] == 4 || m_ArrayLevel[y][x] == 5 || m_ArrayLevel[y][x] == 6 || m_ArrayLevel[y][x] == 7)
			{
				if (postac.getPrawo().intersects(blok))
				{
					postac.stopPrawo(blok.left);
				}
				else if (postac.getLewo().intersects(blok))
				{
					postac.stopLewo(blok.left);
				}

				if (postac.getStopy().intersects(blok))
				{
					postac.stopFall(blok.top);	
					postac.justFell(true);
					postac.isStanding(true);
				}
				else
				{
					postac.isStanding(false);
				}
					
				if (postac.getGlowa().intersects(blok))
				{
					postac.stopJump();
					if (m_ArrayLevel[y][x] == 4)
					{
						m_LevelManager.updateBlock(m_VALevel, y, x);
						m_ArrayLevel[y][x] = 3;
						m_CurrentScore += 10;
					}
				}
			}
			//czy postac osiagnela cel?
			if (m_ArrayLevel[y][x] == 10)
			{
				if (postac.getPosition().intersects(blok))
				{
					m_NewLevelRequired = true;
				}			
			}
		}
	}

	//kontakt z przeciwnikami
	for (int i = 0; i < ENEMIES_NUM; i++)
	{
		//postac skoczyla na glowe przeciwnika
		if (postac.getStopy().intersects(m_Goombas[i].getGlowa()) && m_Goombas[i].isAlive())
		{
			if (m_Goombas[i].hit())
			{
				m_CurrentScore += 20;
			}
		}
		if (postac.getPosition().intersects(m_Goombas[i].getPosition()))
		{
			if (m_Goombas[i].isAlive())
			{
				if (postac.updateHealth(-10, m_GameTimeTotal.asSeconds()))
				{
					m_State = State::GAME_OVER;
				}
			}
		}
	}

	return koniecPoziomuOsiagniety;
}