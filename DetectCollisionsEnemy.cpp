#include "stdafx.h"
#include "Engine.h"

void Engine::detectCollisionsEnemy(Enemies& enemy)
{
	FloatRect detectionZone = enemy.getPosition();

	//floatrect pojedynczego bloku aby mozna bylo wykrywac kolizje
	FloatRect blok;

	blok.width = TILE_SIZE;
	blok.height = TILE_SIZE;

	//strefa w ktorej bedzie sie szukalo kolizji
	int startX = (int)(detectionZone.left / TILE_SIZE) - 2;
	int endX = (int)(detectionZone.left / TILE_SIZE) + 3;

	int startY = (int)(detectionZone.top / TILE_SIZE) - 2;
	int endY = (int)(detectionZone.top / TILE_SIZE) + 3;

	//detection zone nie moze wychodzic poza poziom (inaczej si� skraszuje)
	if (startX < 0) startX = 0;
	if (startY < 0)startY = 0;
	if (endX >= m_LevelManager.getLevelSize().x) endX = m_LevelManager.getLevelSize().x;
	if (endY >= m_LevelManager.getLevelSize().y) endY = m_LevelManager.getLevelSize().y;

	//czy postac spadla poza mape
	FloatRect level(0, 0, m_LevelManager.getLevelSize().x * TILE_SIZE, m_LevelManager.getLevelSize().y * TILE_SIZE);

	//zderzenie pocisku z przeciwnikiem
	for (int i = 0; i < BULLETS_NUM; i++)
	{
		if (m_Bullets[i].isInFlight() && enemy.isAlive())
		{
			if ((m_Bullets[i].getPosition().intersects(enemy.getPosition())))
			{
				m_Bullets[i].stop();
				if (enemy.hit())
				{
					m_CurrentScore += 20;
				}
			}
		}
	}
	if (!enemy.getPosition().intersects(level))
	{
		//wypadl poza poziom, ded
		enemy.setAlive(false);
	}

	// detection zone do wykrywania, czy gracz jest w poblizu szesciu blokow
	detectionZone.left -= TILE_SIZE * 6;
	detectionZone.top -= TILE_SIZE * 6;
	detectionZone.width += TILE_SIZE * 6 * 2;
	detectionZone.height += TILE_SIZE * 6 * 2;

	for (int x = startX; x < endX; x++)
	{
		for (int y = startY; y < endY; y++)
		{
			blok.left = x * TILE_SIZE;
			blok.top = y * TILE_SIZE;

			//czy postac koliduje z blokiem
			if (m_ArrayLevel[y][x] == 1 || m_ArrayLevel[y][x] == 2 || m_ArrayLevel[y][x] == 3 || m_ArrayLevel[y][x] == 4 || m_ArrayLevel[y][x] == 5 || m_ArrayLevel[y][x] == 6 ||
				m_ArrayLevel[y][x] == 7 )
			{
				if (enemy.getPrawo().intersects(blok))
				{
					enemy.stopPrawo(blok.left);
					if (!enemy.isRoaming())
					{
						//zr�b, zeby caly czas wysy�a�o "jump" kiedy dotknie sciany przez jakis czas(jakis timer zrob czy cos)
						enemy.jump();
					}
				}
				else if (enemy.getLewo().intersects(blok))
				{
					enemy.stopLewo(blok.left);
					if (!enemy.isRoaming())
					{
						m_JumpForASecond = true;
					}
				}

				if (enemy.getStopy().intersects(blok))
				{
					enemy.stopFall(blok.top);
					enemy.justFell(true);
					enemy.isStanding(true);
				}
				else
				{
					enemy.isStanding(false);
				}

				if (enemy.getGlowa().intersects(blok))
				{
					enemy.stopJump();
				}
			}

			//kiedy przeciwnik sie wloczy, odbijaj od przeszkod
			if (enemy.isRoaming())
			{
				if (m_ArrayLevel[y][x] == 1 || m_ArrayLevel[y][x] == 2 || m_ArrayLevel[y][x] == 3 || m_ArrayLevel[y][x] == 4 || m_ArrayLevel[y][x] == 5 || m_ArrayLevel[y][x] == 6 || m_ArrayLevel[y][x] == 7 || m_ArrayLevel[y][x] == 8)
				{
					if (enemy.getLewo().intersects(blok))
					{
						enemy.changeDirection(1);
					}
					else
						if (enemy.getPrawo().intersects(blok))
						{
							enemy.changeDirection(0);
						}
				}
			}

			//jezeli w zasiegu jest gracz, przestan sie wloczyc i go atakuj, ale tylko od drugiego poziomu
			if (m_LevelManager.getCurrentLevel() == 2 || m_LevelManager.getCurrentLevel() == 3)
			{
				if (m_MarioActive)
				{
					if (detectionZone.intersects(m_Mario.getPosition()))
					{
						enemy.attack();
					}
				}
				else
				{
					if (detectionZone.intersects(m_Luigi.getPosition()))
					{
						enemy.attack();
					}
				}
			}
		}
	}
}