#include "stdafx.h"
#include "Engine.h"
#include <sstream>

#include "Postacie.h"

void Engine::draw()
{
	m_Window.clear(Color::Black);

	m_Window.setView(m_BGMainView);
	switch (m_LevelManager.getCurrentLevel())
	{
	case 1:
		m_Window.draw(m_BGSpr1);
		break;
	case 2:
		m_Window.draw(m_BGSpr2);
		break;
	case 3:
		m_Window.draw(m_BGSpr3);
		break;
	}
	
	m_Window.setView(m_MainView);
	m_Window.draw(m_VALevel, &m_TextureTiles);

	for (int i = 0; i < BULLETS_NUM; i++)
	{
		if (m_Bullets[i].isInFlight())
		{
			m_Window.draw(m_Bullets[i].getSprite());
		}
	}
	
	for (int i = 0; i < ENEMIES_NUM; i++)
	{
		if (m_Goombas[i].isActive())
		{
			m_Window.draw(m_Goombas[i].getSprite());
		}
	}
		if (m_MarioActive)
		{
			m_Window.draw(m_Mario.getSprite());
			if (m_Hitboxes)
			{
				m_Window.draw(m_Mario.getStopyShape());
				m_Window.draw(m_Mario.getGlowaShape());
				m_Window.draw(m_Mario.getLewoShape());
				m_Window.draw(m_Mario.getPrawoShape());
				for (int i = 0; i < ENEMIES_NUM; i++)
				{
					if (m_Goombas[i].isAlive())
					{
						m_Window.draw(m_Goombas[i].getStopyShape());
						m_Window.draw(m_Goombas[i].getGlowaShape());
						m_Window.draw(m_Goombas[i].getLewoShape());
						m_Window.draw(m_Goombas[i].getPrawoShape());
					}
				}
			}
		}
		else
		{
			m_Window.draw(m_Luigi.getSprite());
			if (m_Hitboxes)
			{
				m_Window.draw(m_Luigi.getStopyShape());
				m_Window.draw(m_Luigi.getGlowaShape());
				m_Window.draw(m_Luigi.getLewoShape());
				m_Window.draw(m_Luigi.getPrawoShape());
				for (int i = 0; i < ENEMIES_NUM; i++)
				{
					if (m_Goombas[i].isAlive())
					{
						m_Window.draw(m_Goombas[i].getStopyShape());
						m_Window.draw(m_Goombas[i].getGlowaShape());
						m_Window.draw(m_Goombas[i].getLewoShape());
						m_Window.draw(m_Goombas[i].getPrawoShape());
					}
				}
			}
		}

	if (m_IsGunActive == 1 || m_IsGunActive == 2)
	{
		if (m_MarioActive)
		{
			//mario jest aktywny, zwroc rewolwer z ramieniem Mario
			m_Window.draw(m_Revolver.getSprite(true));
		}
		else
		{
			//mario nie jest aktywny, czyli luigi jest. zwroc bron z jego ramianiem
			m_Window.draw(m_Revolver.getSprite(false));
		}
	}

	m_Window.setView(m_BGMainView);

	if (m_State == State::START_GAME)
	{
		m_BGMainView.reset(FloatRect(0, 0, VideoMode::getDesktopMode().width, VideoMode::getDesktopMode().height));
		m_Window.draw(m_BGSpriteStart);
	}
	if (m_State == State::CHOOSE_CHAR)
	{
		m_BGMainView.reset(FloatRect(0, 0, VideoMode::getDesktopMode().width, VideoMode::getDesktopMode().height));
		m_Window.draw(m_BGSpriteChoose);
	}

	if (m_State == State::GAME_OVER)
	{
		m_BGMainView.reset(FloatRect(0, 0, VideoMode::getDesktopMode().width, VideoMode::getDesktopMode().height));
		m_Window.draw(m_GameOverSpr);
	}

	if (m_State == State::WON)
	{
		m_BGMainView.reset(FloatRect(0, 0, VideoMode::getDesktopMode().width, VideoMode::getDesktopMode().height));
		m_Window.draw(m_WinSpr);

		m_Window.setView(m_HudView);

		m_Window.draw(m_Hud.getLev1Time());
		m_Window.draw(m_Hud.getLev2Time());
		m_Window.draw(m_Hud.getLev3Time());
		m_Window.draw(m_Hud.getScoreWin());
	}

	if (m_State == State::PLAYING)
	{
		//rysuj HUD
		//przelacz view na m_HudView
		m_Window.setView(m_HudView);

		m_Window.draw(m_Hud.getHealth());
		m_Window.draw(m_Hud.getHealthBar());
		m_Window.draw(m_Hud.getLevel());
		m_Window.draw(m_Hud.getTime());
		m_Window.draw(m_Hud.getScore());
	}
	m_Window.display();
}