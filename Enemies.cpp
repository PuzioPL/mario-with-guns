#include "stdafx.h"
#include "Enemies.h"

bool Enemies::hit()
{
	m_Health--;

	if (m_Health < 0)
	{
		//dead
		m_Alive = false;
		m_Sprite.setTextureRect(IntRect(0, 102, 50, 51));
		m_IsFalling = true;
		return true;
	}
	else
	{
		return false;
	}
}

void Enemies::spawn(Vector2f startPosition)
{
	m_Position.x = startPosition.x;
	m_Position.y = startPosition.y;

	m_Sprite.setPosition(m_Position);

	m_Source.x = 0;
	m_Source.y = 0;

	m_Alive = true;
	m_Active = true;
	m_Roaming = true;
	m_Health = 2;
	m_Speed = 150;
}

FloatRect Enemies::getPosition()
{
	return m_Sprite.getGlobalBounds();
}

FloatRect Enemies::getStopy()
{
	return m_Stopy;
}

FloatRect Enemies::getGlowa()
{
	return m_Glowa;
}

FloatRect Enemies::getPrawo()
{
	return m_Prawo;
}

FloatRect Enemies::getLewo()
{
	return m_Lewo;
}

Sprite Enemies::getSprite()
{
	return m_Sprite;
}

Vector2f Enemies::getCenter()
{
	return Vector2f(m_Position.x + m_Sprite.getGlobalBounds().width / 2, m_Position.y + m_Sprite.getGlobalBounds().height / 2);
}

//gettery na hitboxy
RectangleShape Enemies::getStopyShape()
{
	return m_StopyShape;
}

RectangleShape Enemies::getGlowaShape()
{
	return m_GlowaShape;
}

RectangleShape Enemies::getPrawoShape()
{
	return m_PrawoShape;
}

RectangleShape Enemies::getLewoShape()
{
	return m_LewoShape;
}

void Enemies::setAlive(bool i)
{
	m_Alive = i;
}

bool Enemies::isAlive()
{
	return m_Alive;
}

void Enemies::setActive(bool i)
{
	m_Active = i;
}

void Enemies::stopFall(float position)
{
	m_Position.y = position - getPosition().height; 
	m_Sprite.setPosition(m_Position); 
    m_IsFalling = false;
}

void Enemies::stopPrawo(float position)
{
	m_Position.x = position - m_Sprite.getGlobalBounds().width;
	m_Sprite.setPosition(m_Position);
	m_MoveRight = false;
	m_MoveLeft = false;
}

void Enemies::stopLewo(float position)
{
	m_Position.x = position + m_Sprite.getGlobalBounds().width;
	m_Sprite.setPosition(m_Position);
	m_MoveRight = false;
	m_MoveLeft = false;
}

void Enemies::stopJump()
{
	m_IsJumping = false;
	m_IsFalling = true;
}

bool Enemies::isActive()
{
	return m_Active;
}



void Enemies::handle()
{
	if (Keyboard::isKeyPressed(Keyboard::C))
	{
		//wykonaj skok o ile nie jest w trakcie skoku, ale tylko kiedy nie spada (stoi na klocku)
		jump();
	}
	else
	{
		m_IsJumping = false;
		m_IsFalling = true;
	}
}

void Enemies::jump()
{
	if (!m_IsJumping && !m_IsFalling)
	{
		m_IsJumping = true;
		m_JustJumped = true;
	}
}

void Enemies::justFell(bool i)
{
	m_JustFell = i;
	if (m_JustFell == true)
	{
		m_Przysp = 0.8;
	}
}

void Enemies::isStanding(bool i)
{
	m_IsStanding = i;
}

void Enemies::changeDirection(int i) // 0 - w lewo, 1 - w prawo
{
	if (i == 0)
	{
		m_MoveLeft = true;
		m_MoveRight = false;
		m_Source.y = 0;
	}
	else
	{
		m_MoveLeft = false;
		m_MoveRight = true;
		m_Source.y = 1;
	}
}

bool Enemies::isRoaming()
{
	return m_Roaming;
}

void Enemies::attack()
{
	m_Roaming = false;
	m_Speed = 250;
}