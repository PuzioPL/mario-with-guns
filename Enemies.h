#pragma once
#include <SFML/Graphics.hpp>

using namespace sf;

class Enemies
{
protected:
	Sprite m_Sprite;

	bool m_Alive = true;
	bool m_Active; // czy przeciwnik w ogole istnieje

	Vector2i m_Source; // do animacji sprite
	float m_Czas; // tez do animacji
	float m_Speed;
	float m_Health;

	bool m_IsJumping;
	bool m_IsFalling = true;
	bool m_Roaming;

	Vector2f m_Position;

	bool m_MoveLeft;
	bool m_MoveRight;
	float m_TimeThisJump;
	bool m_JustJumped; //do odtwarzania dzwieku
	bool m_JustFell;
	float m_Przysp = 0.8;
	float m_StalaSkoku;
	float m_SzybkoscSkoku = 0.5;
	bool m_IsStanding = false;


	FloatRect m_Stopy;
	FloatRect m_Glowa;
	FloatRect m_Prawo;
	FloatRect m_Lewo;

	//hitboxy
	RectangleShape m_StopyShape;
	RectangleShape m_GlowaShape;
	RectangleShape m_PrawoShape;
	RectangleShape m_LewoShape;

	float m_JumpDuration;

private:
	

	Texture m_Texture;
public:
	bool hit();
	bool isAlive();
	bool isActive();
	void setAlive(bool i);
	void setActive(bool i);
	void spawn(Vector2f startPosition);
	Vector2f getCenter();

	void virtual update(float dtAsSeconds, Vector2f playerPosition) = 0; // kazdy przeciwnik bedzie mial wlasne zachowanie

	FloatRect getPosition();
	Sprite getSprite();

	void jump();
	
	FloatRect getStopy();
	FloatRect getGlowa();
	FloatRect getPrawo();
	FloatRect getLewo();

	void stopFall(float position);
	void stopPrawo(float position);
	void stopLewo(float position);
	void stopJump();

	//hiboxy
	RectangleShape getStopyShape();
	RectangleShape getGlowaShape();
	RectangleShape getPrawoShape();
	RectangleShape getLewoShape();

	void justFell(bool i);
	void isStanding(bool i);
	//************
	void handle();

	void changeDirection(int i);
	bool isRoaming();
	void attack();
};