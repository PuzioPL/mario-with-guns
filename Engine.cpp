#include "stdafx.h"
#include "Engine.h"

Engine::Engine()
{
	//stworz okno sfml dla rozdzielczosci ekranu
	Vector2f rozdzielczosc;
	rozdzielczosc.x = VideoMode::getDesktopMode().width;
	rozdzielczosc.y = VideoMode::getDesktopMode().height;

	m_Window.create(VideoMode(rozdzielczosc.x, rozdzielczosc.y), "Mario", Style::Fullscreen);
	//m_Window.setFramerateLimit(100);

	m_MainView.setSize(rozdzielczosc);
	m_BGMainView.reset(FloatRect(0, 0, rozdzielczosc.x, rozdzielczosc.y));
	m_HudView.reset(FloatRect(0, 0, rozdzielczosc.x, rozdzielczosc.y));

	m_TextureTiles = TextureHolder::GetTexture("graphics/tiles_sheet.png");
	
	m_BGTextureStart = TextureHolder::GetTexture("graphics/bg_start.png");
	m_BGTextureChoose = TextureHolder::GetTexture("graphics/bg_choose_char.png");

	m_BGSpriteStart.setTexture(m_BGTextureStart);
	m_BGSpriteStart.setPosition(0,0);

	m_BGSpriteChoose.setTexture(m_BGTextureChoose);
	m_BGSpriteChoose.setPosition(0,0);

	m_BGTxt1 = TextureHolder::GetTexture("graphics/tlo1.png");
	m_BGSpr1.setTexture(m_BGTxt1);
	m_BGSpr1.setPosition(-950, 0);

	m_BGTxt2 = TextureHolder::GetTexture("graphics/tlo2.png");
	m_BGSpr2.setTexture(m_BGTxt2);
	m_BGSpr2.setPosition(-950, 0);

	m_BGTxt3 = TextureHolder::GetTexture("graphics/tlo3.png");
	m_BGSpr3.setTexture(m_BGTxt3);
	m_BGSpr3.setPosition(-950, 0);

	m_GameOverTxt = TextureHolder::GetTexture("graphics/gameover.png");
	m_GameOverSpr.setTexture(m_GameOverTxt);
	m_GameOverSpr.setPosition(0, 0);

	m_WinTxt = TextureHolder::GetTexture("graphics/bg_win.png");
	m_WinSpr.setTexture(m_WinTxt);
	m_WinSpr.setPosition(0, 0);
}

void Engine::run()
{
	//czas
	Clock clock;

	int i = 0;
	while (m_Window.isOpen())
	{
		//resetuje zegar i zwraca czas jaki uplynal
		Time dt = clock.restart();

		m_GameTimeTotal += dt;
		
		float dtAsSeconds = dt.asSeconds();
		input();
		update(dtAsSeconds);
		draw();
	}

}
