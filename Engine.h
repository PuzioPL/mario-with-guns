#pragma once
#include <SFML/Graphics.hpp>
#include "LevelManager.h"
#include "TextureHolder.h"
#include "Mario.h"
#include "Luigi.h"
#include "Revolver.h"
#include "Bullet.h"
#include "SoundManager.h"
#include "Goomba.h"
#include "HUD.h"

using namespace sf;

class Engine
{
private:
	TextureHolder th;
	bool m_NewLevelRequired = true;

	enum class State{PAUSED, START_GAME, CHOOSE_CHAR, PLAYING, GAME_OVER, WON};
	State m_State = State::START_GAME;

	RenderWindow m_Window;

	//HUD
	HUD m_Hud;
	int m_FramesSinceLastHUDUpdate = 0;
	int m_TargetFramesPerHUDUpdate = 100;

	int m_CurrentScore = 0;
	int m_HiScore;

	Vector2f m_MouseWorldPosition;
	const int BULLETS_NUM = 100;
	const int ENEMIES_NUM = 40;
	Bullet m_Bullets[100];
	Goomba m_Goombas[40];
	int m_CurrentBullet = 0;
	float m_FireRate = 4;
	// kiedy byl ostatni wystrzal z broni (do okreslenia jej szybkostrzelnosci)
	Time m_LastPressed;

	Mario m_Mario;
	Luigi m_Luigi;
	bool m_Hitboxes = false;
	bool m_MarioActive = true;
	int m_IsGunActive = 0;
	int m_Facing; // w ktora strona postac jest skierowana: 0 - lewo, 1 - prawo, 2 - lewo skok, 3 - prawo skok


	Revolver m_Revolver;

	LevelManager m_LevelManager;
	VertexArray m_VALevel;
	//podwojny wskaznik na dwuwymiarowa tablice
	int** m_ArrayLevel = NULL;
	Texture m_TextureTiles;

	//do rotacji pociskow
	Vector2f m_MousePositionWhenClicked;

	View m_MainView;

	View m_BGMainView;
	View m_HudView;

	Texture m_BGTxt1;
	Sprite m_BGSpr1;

	Texture m_BGTxt2;
	Sprite m_BGSpr2;

	Texture m_BGTxt3;
	Sprite m_BGSpr3;

	Texture m_GameOverTxt;
	Sprite m_GameOverSpr;

	Texture m_WinTxt;
	Sprite m_WinSpr;

	Texture m_BGTextureStart;
	Texture m_BGTextureChoose;

	Sprite m_BGSpriteStart;
	Sprite m_BGSpriteChoose;

	Time m_GameTimeTotal;

	float m_GameTimeLevelSec = 0;
	float m_GameTimeLevelMin = 0;

	void input();
	void update(float dtAsSeconds);
	void draw();

	void loadLevel();
	
	const int GRAVITY = 300;
	const int TILE_SIZE = 50;
	const int VERTS_IN_QUAD = 4;

	bool detectCollisionsChar(Postacie& postac, float dtAsSeconds); //polimorficzny argument

	void detectCollisionsEnemy(Enemies& enemy); //polimorficzny argument

	void detectCollisionsBullet(Bullet& bullet);

	bool m_BlockUpdate = false;

	SoundManager m_SM;

	//skok przeciwnika
	bool m_JumpForASecond = false;

public:
	Engine();

	void run();
};