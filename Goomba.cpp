#include "stdafx.h"
#include "Goomba.h"
#include "TextureHolder.h"

Goomba::Goomba()
{
	m_Sprite = Sprite(TextureHolder::GetTexture("graphics/goomba_sheet.png"));
	m_Sprite.setTextureRect(sf::IntRect(0, 0, 50, 51));
	m_Health = 2;
	m_Speed = 150;

	m_StalaSkoku = 5;
	//m_Sprite.setOrigin(25, 25);
	
	//przeciwnicy spawnuja sie w trybie roaming 
	m_MoveLeft = true;
	m_MoveRight = false;
	m_Roaming = true;
}

void Goomba::update(float dtAsSeconds, Vector2f playerPosition)
{
	if (m_Active)
	{
		if (m_Alive)
		{
			float playerX = playerPosition.x;
			float playerY = playerPosition.y;

			if (m_Roaming)
			{
				
			}
			else
			{
				//aktualizuj polozenie wzgleden gracza
				if (playerX > m_Position.x)
				{
					m_MoveRight = true;
					m_MoveLeft = false;
				}
				if (playerX < m_Position.x)
				{
					m_MoveLeft = true;
					m_MoveRight = false;
				}
			}
			//animacja sprite
			m_Czas += dtAsSeconds;
			if (m_Czas > 0.25)
			{
				m_Source.x++;
				if (m_Source.x * 50 >= 100)
				{
					m_Source.x = 0;
				}
				m_Czas = 0;

				m_Sprite.setTextureRect(sf::IntRect(m_Source.x * 50, m_Source.y * 52, 50, 51));
			}

			if (m_MoveLeft)
			{
				m_Position.x = m_Position.x - m_Speed * dtAsSeconds;
				m_Source.y = 0;
			}
			if (m_MoveRight)
			{
				m_Position.x = m_Position.x + m_Speed * dtAsSeconds;
				m_Source.y = 1;
			}

			if (m_IsJumping)
			{
				if (m_JustJumped)
				{
					m_SzybkoscSkoku = m_StalaSkoku;
					m_JustJumped = false;
					m_IsFalling = false;
				}

				m_Position.y -= m_SzybkoscSkoku;
				if (m_SzybkoscSkoku > 0)
				{
					m_SzybkoscSkoku -= 0.065 * m_Przysp;
					m_Przysp -= 0.0035;
				}

				else
				{
					m_IsJumping = false;
					m_IsFalling = true;
				}
			}

			if (m_IsFalling)
			{
				if (m_JustFell)
				{
					m_SzybkoscSkoku = 0.5;
					m_JustFell = false;
				}
				m_Position.y += m_SzybkoscSkoku;
				if (m_SzybkoscSkoku < m_StalaSkoku)
				{
					m_SzybkoscSkoku += 0.065 * m_Przysp;
					m_Przysp += 0.0035;
				}
			}

			//hitbox
			FloatRect r = getPosition();

			//stopy
			m_Stopy.left = r.left + 15;
			m_Stopy.top = r.top + r.height - 1;
			m_Stopy.width = r.width - 30;
			m_Stopy.height = 1;

			//glowa
			m_Glowa.left = r.left + 3;
			m_Glowa.top = r.top - 20;
			m_Glowa.width = r.width - 6;
			m_Glowa.height = 35;

			//prawo
			m_Prawo.left = r.left + r.width - 2;
			m_Prawo.top = r.top + 15;
			m_Prawo.width = 1;
			m_Prawo.height = r.height - 30;

			//lewo
			m_Lewo.left = r.left;
			m_Lewo.top = r.top + 15;
			m_Lewo.width = 1;
			m_Lewo.height = r.height - 30;

			m_Sprite.setPosition(m_Position);

			//*************************************************************************************************************
			//prostokaty ktore pokazuja hitboxy
			m_StopyShape.setSize(Vector2f(m_Stopy.width, m_Stopy.height));
			m_StopyShape.setPosition(Vector2f(m_Stopy.left, m_Stopy.top));
			m_StopyShape.setFillColor(Color::Red);

			m_GlowaShape.setSize(Vector2f(m_Glowa.width, m_Glowa.height));
			m_GlowaShape.setPosition(Vector2f(m_Glowa.left, m_Glowa.top));
			m_GlowaShape.setFillColor(Color::Green);

			m_PrawoShape.setSize(Vector2f(m_Prawo.width, m_Prawo.height));
			m_PrawoShape.setPosition(Vector2f(m_Prawo.left, m_Prawo.top));
			m_PrawoShape.setFillColor(Color::Yellow);

			m_LewoShape.setSize(Vector2f(m_Lewo.width, m_Lewo.height));
			m_LewoShape.setPosition(Vector2f(m_Lewo.left, m_Lewo.top));
			m_LewoShape.setFillColor(Color::Blue);
		}
		else
		{
			if (m_IsFalling)
			{
				if (m_JustFell)
				{
					m_SzybkoscSkoku = 0.5;
					m_JustFell = false;
				}
				m_Position.y += m_SzybkoscSkoku;
				if (m_SzybkoscSkoku < m_StalaSkoku)
				{
					m_SzybkoscSkoku += 0.065 * m_Przysp;
					m_Przysp += 0.0035;
				}
			}
		}
	}
	else
	{
		m_Position = Vector2f(-1000, 0);
	}
	m_Sprite.setPosition(m_Position);
}