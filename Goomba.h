#pragma once
#include "Enemies.h"

class Goomba : public Enemies
{
public:
	Goomba::Goomba();

	void virtual update(float dtAsSeconds, Vector2f playerPosition);
};