#include "stdafx.h"
#include "Guns.h"

void Guns::spawn(Vector2f startPosition)
{
	m_Position.x = startPosition.x;
	m_Position.y = startPosition.y;

	m_SpriteRevMario.setPosition(m_Position);
	m_SpriteRevLuigi.setPosition(m_Position);

	m_Resolution.x = VideoMode::getDesktopMode().width;
	m_Resolution.y = VideoMode::getDesktopMode().height;
}

void Guns::update(float elapsedTime, Vector2f mousePosition, Vector2f characterPosition)
{
	m_Position.x = characterPosition.x;
	m_Position.y = characterPosition.y;
	
	//ustawiam pozycje obu broni, ale rysowana bedzie jedna, zaleznie od aktywnej postaci
	m_SpriteRevLuigi.setPosition(m_Position);
	m_SpriteRevMario.setPosition(m_Position);

	float angle = (atan2(mousePosition.y - m_Position.y , mousePosition.x - m_Position.x ) * 180) / 3.141;

	if (angle > 90 || angle < -90)
	{
		m_SpriteRevMario.setTextureRect(sf::IntRect(0, 36, 73, 36));
		m_SpriteRevLuigi.setTextureRect(sf::IntRect(0, 36, 73, 36));
	}
	else
	{
		m_SpriteRevMario.setTextureRect(sf::IntRect(0, 0, 73, 36));
		m_SpriteRevLuigi.setTextureRect(sf::IntRect(0, 0, 73, 36));
	}
	m_SpriteRevMario.setRotation(angle);
	m_SpriteRevLuigi.setRotation(angle);
}

FloatRect Guns::getPosition()
{
	return m_SpriteRevMario.getGlobalBounds();
}

//DAJ, ZEBY TA FUNKCJA BRA�A ZA PARAMETR 1 LUB 0, ZALEZNIE OD TEGO KTORA POSTAC JEST AKTYWNA@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Sprite Guns::getSprite(bool isMarioActive)
{
	if (isMarioActive)
	{
		return m_SpriteRevMario;
	}
	else
	{
		return m_SpriteRevLuigi;
	}
}

Vector2f Guns::getCenter()
{
	return Vector2f(m_Position.x + m_SpriteRevMario.getGlobalBounds().width / 2, m_Position.y + m_SpriteRevMario.getGlobalBounds().height / 2);
}
