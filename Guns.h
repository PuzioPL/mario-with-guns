#pragma once
#include <SFML/Graphics.hpp>

using namespace sf;

class Guns
{
protected:
	//sprite'y rewolwera dla mario i luigi
	Sprite m_SpriteRevMario;
	Sprite m_SpriteRevLuigi;

	Vector2f m_Resolution;
	bool m_JustShot;

private:
	Vector2f m_Position;
	
	Texture m_TextureRevMario;
	Texture m_TextureRevLuigi;
public:
	void spawn(Vector2f position);

	bool virtual handleInput() = 0;

	FloatRect getPosition();

	//getSprite zwraca sprite z ramieniem mario lub luigiego, zaleznie od parametru
	Sprite getSprite(bool isMarioActive);
	Vector2f getCenter();

	void update(float elapsedTime, Vector2f mousePosition, Vector2f characterPosition);
};