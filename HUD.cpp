#include "stdafx.h"
#include "HUD.h"

HUD::HUD()
{
	Vector2u rozdzielczosc;
	rozdzielczosc.x = VideoMode::getDesktopMode().width;
	rozdzielczosc.y = VideoMode::getDesktopMode().height;

	//zaladuj czcionke
	m_Font.loadFromFile("fonts/SuperMario256.ttf");

	/*
	Text m_TimeText;
	Text m_ScoreText;
	Text m_LevelText;
	*/

	// aktualy poziom - lewy gorny rog
	m_LevelText.setFont(m_Font);
	m_LevelText.setCharacterSize(40);
	m_LevelText.setFillColor(Color::Black);
	m_LevelText.setPosition(25, 0);
	m_LevelText.setString("1");

	// licznik czasu, ponizej aktualnego poziomu
	m_TimeText.setFont(m_Font);
	m_TimeText.setCharacterSize(40);
	m_TimeText.setFillColor(Color::Black);
	m_TimeText.setPosition(25, 50);
	m_TimeText.setString("-----");

	// licznik score
	m_ScoreText.setFont(m_Font);
	m_ScoreText.setCharacterSize(40);
	m_ScoreText.setFillColor(Color::Black);
	m_ScoreText.setPosition(rozdzielczosc.x - 300, 0);
	m_ScoreText.setString("-----");

	//health bar
	m_HealthBar.setFillColor(Color::Red);
	m_HealthBar.setPosition(100, 920);

	//health
	m_Health.setFont(m_Font);
	m_Health.setCharacterSize(40);
	m_Health.setFillColor(Color::Black);
	m_Health.setPosition(100, 850);
	m_Health.setString("-----");

	//czasy poszczegolnych poziomow
	m_Lev1Time.setFont(m_Font);
	m_Lev1Time.setCharacterSize(40);
	m_Lev1Time.setFillColor(Color::Black);
	m_Lev1Time.setPosition(500, 400);
	m_Lev1Time.setString("-----");

	m_Lev2Time.setFont(m_Font);
	m_Lev2Time.setCharacterSize(40);
	m_Lev2Time.setFillColor(Color::Black);
	m_Lev2Time.setPosition(500, 500);
	m_Lev2Time.setString("-----");

	m_Lev3Time.setFont(m_Font);
	m_Lev3Time.setCharacterSize(40);
	m_Lev3Time.setFillColor(Color::Black);
	m_Lev3Time.setPosition(500, 600);
	m_Lev3Time.setString("-----");

	//score win
	m_ScoreWin.setFont(m_Font);
	m_ScoreWin.setCharacterSize(40);
	m_ScoreWin.setFillColor(Color::Black);
	m_ScoreWin.setPosition(1200, 500);
	m_ScoreWin.setString("-----");
}

Text HUD::getLevel()
{
	return m_LevelText;
}

Text HUD::getTime()
{
	return m_TimeText;
}

Text HUD::getScore()
{
	return m_ScoreText;
}

void HUD::setLevel(String text)
{
	m_LevelText.setString(text);
}

void HUD::setTime(String text)
{
	m_TimeText.setString(text);
}

void HUD::setScore(String text)
{
	m_ScoreText.setString(text);
}

void HUD::setHealthBar(int i)
{
	m_HealthBar.setSize(Vector2f(i * 3, 50));
}

RectangleShape HUD::getHealthBar()
{
	return m_HealthBar;
}

void HUD::setHealth(String text)
{
	m_Health.setString(text);
}

Text HUD::getHealth()
{
	return m_Health;
}

Text HUD::getLev1Time()
{
	return m_Lev1Time;
}

Text HUD::getLev2Time()
{
	return m_Lev2Time;
}

Text HUD::getLev3Time()
{
	return m_Lev3Time;
}


void HUD::setLevel1Time(String text)
{
	m_Lev1Time.setString(text);
}

void HUD::setLevel2Time(String text)
{
	m_Lev2Time.setString(text);
}

void HUD::setLevel3Time(String text)
{
	m_Lev3Time.setString(text);
}

Text HUD::getScoreWin()
{
	return m_ScoreWin;
}

void HUD::setScoreWin(String text)
{
	m_ScoreWin.setString(text);
}