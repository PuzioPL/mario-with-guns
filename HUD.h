#pragma once
#include <SFML/Graphics.hpp>

using namespace sf;

class HUD
{
private:
	Font m_Font;
	Text m_TimeText;
	Text m_ScoreText;
	Text m_LevelText;
	Text m_Health;

	//na koniec pokazuje sie ile czasu zajal kazdy z poziomow oraz ile zajely lacznie
	Text m_YouWin;
	Text m_Lev1Time;
	Text m_Lev2Time;
	Text m_Lev3Time;

	//do pokazywania punktow na koncu poziomu
	Text m_ScoreWin;

	RectangleShape m_HealthBar;

public:
	HUD();
	Text getTime();
	Text getLevel();
	Text getScore();
	Text getHealth();
	
	//czas kazdego poziomu
	Text getLev1Time();
	Text getLev2Time();
	Text getLev3Time();
	Text getScoreWin();

	void setLevel1Time(String text);
	void setLevel2Time(String text);
	void setLevel3Time(String text);
	void setScoreWin(String text);

	RectangleShape getHealthBar();

	void setLevel(String text);
	void setTime(String text);
	void setScore(String text);

	void setHealth(String text);
	void setHealthBar(int health);
};