#include "stdafx.h"
#include "Engine.h"

void Engine::input()
{
	Event event;
	while (m_Window.pollEvent(event))
	{
		if (event.type == Event::KeyPressed)
		{
			if (Keyboard::isKeyPressed(Keyboard::Escape))
			{
				m_Window.close();
			}
			if (Keyboard::isKeyPressed(Keyboard::Return))
			{
				if (m_State == State::PLAYING)
				{
					m_State = State::PAUSED;
				}
				else if (m_State == State::PAUSED)
				{
					m_State = State::PLAYING;
				}
				else if (m_State == State::START_GAME)
				{
					m_State = State::CHOOSE_CHAR;
				}
				else if (m_State == State::WON)
				{
					m_State = State::CHOOSE_CHAR;
				}
				else
				if (m_State == State::GAME_OVER)
				{
					m_State = State::CHOOSE_CHAR;
				}


			}
			if (Keyboard::isKeyPressed(Keyboard::B))
			{
				m_Hitboxes = !m_Hitboxes;
			}

			//nowy poziom, do sprawdzenia czy wszystko smiga
			if (Keyboard::isKeyPressed(Keyboard::K))
			{
				m_NewLevelRequired = true;
			}

			//wybor postaci
			if (m_State == State::CHOOSE_CHAR)
			{
				if (Keyboard::isKeyPressed(Keyboard::Num1))
				{
					m_MarioActive = true;
					m_State = State::PLAYING;
					m_LevelManager.setCurrentLevel(0);
					m_NewLevelRequired = true;
				}

				if (Keyboard::isKeyPressed(Keyboard::Num2))
				{
					m_MarioActive = false;
					m_State = State::PLAYING;
					m_LevelManager.setCurrentLevel(0);
					m_NewLevelRequired = true;
				}
			}
			if (Keyboard::isKeyPressed(Keyboard::N))
			{
				m_IsGunActive++;
				if (m_IsGunActive >= 3)
				{
					m_IsGunActive = 0;
				}
			}

		}
		if (m_State == State::PLAYING)
		{
			if (Mouse::isButtonPressed(Mouse::Left))//(Keyboard::isKeyPressed(Keyboard::V))
			{
				if (m_IsGunActive == 1 || m_IsGunActive == 2)
				{
					if (m_GameTimeTotal.asMilliseconds() - m_LastPressed.asMilliseconds() > 1000 / m_FireRate)
					{
						//wyszczel pocisk
						//przeka� pozycje gracza oraz pozycje kursora do funkcji "shoot"
						if (m_MarioActive)
						{
							m_Bullets[m_CurrentBullet].shoot(m_Mario.getCenter(), m_MouseWorldPosition.x, m_MouseWorldPosition.y, m_MouseWorldPosition);
						}
						else
						{
							m_Bullets[m_CurrentBullet].shoot(m_Luigi.getCenter(), m_MouseWorldPosition.x, m_MouseWorldPosition.y, m_MouseWorldPosition);
						}
						m_CurrentBullet++;
						if (m_CurrentBullet >= BULLETS_NUM)
						{
							m_CurrentBullet = 0;
						}
						m_LastPressed = m_GameTimeTotal;

						//odtworz dzwiek
						m_SM.playShoot();
					}
				}
			}
		}
	}

	if (m_MarioActive)
	{
		if (m_Mario.handleInput())
		{
			//pusc dzwiek skoku
		}
	}
	else
	{
		if (m_Luigi.handleInput())
		{
			//pusc dzwiek skoku

		}
	}

	if (m_Revolver.handleInput())
	{
		//odtworz dzwiek
	};

	for (int i = 0; i < ENEMIES_NUM; i++)
	{
		m_Goombas[i].handle();
	}
}