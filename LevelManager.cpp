#include "stdafx.h"
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "TextureHolder.h"
#include <sstream>
#include <fstream>
#include "LevelManager.h"

using namespace sf;
using namespace std;

int** LevelManager::nextLevel(VertexArray& rVaLevel)
{
	m_LevelSize.x = 0;
	m_LevelSize.y = 0;

	m_CurrentLevel++;
	if (m_CurrentLevel > NUM_LEVELS)
	{
		m_CurrentLevel = 1;
	}

	string levelToLoad;
	switch (m_CurrentLevel)
	{
	case 1:
		levelToLoad = "levels/level1.txt";
		m_StartPosition.x = 1100;
		m_StartPosition.y = 200;
		break;
	case 2:
		levelToLoad = "levels/level2.txt";
		m_StartPosition.x = 120;
		m_StartPosition.y = 200;
		break;
	case 3:
		levelToLoad = "levels/level3.txt";
		m_StartPosition.x = 120;
		m_StartPosition.y = 600;
		break;
	}

	ifstream input(levelToLoad);
	string s;

	//policz ilosc wierszy
	while (getline(input, s))
	{
		m_LevelSize.y++;
	}

	//ilosc kolumn
	m_LevelSize.x = s.length();

	//wroc na poczatek
	input.clear();
	input.seekg(0, ios::beg);

	//przygotuj tablice dwuwymiarowa
	int** arrayLevel = new int*[m_LevelSize.y];
	for (int i = 0; i < m_LevelSize.y; i++)
	{
		//dodaj nowa tablice do kazdego elementu tablicy
		arrayLevel[i] = new int[m_LevelSize.x];
	}

	string wiersz;
	int y = 0;
	while (input >> wiersz)
	{
		for (int x = 0; x < wiersz.length(); x++)
		{
			const char val = wiersz[x];
			if (val == 'A')
			{
				arrayLevel[y][x] = 10;
			}
			else
			{
				arrayLevel[y][x] = atoi(&val); //atoi zamienia char na int
			}
		}
		y++;
	}

	input.close();

	rVaLevel.setPrimitiveType(Quads);

	//ustaw wielksoc tablicy vertexow
	rVaLevel.resize(m_LevelSize.x * m_LevelSize.y * VERTS_IN_QUAD);
	
	int aktualnyVertex = 0;

	for (int x = 0; x < m_LevelSize.x; x++)
	{
		for (int y = 0; y < m_LevelSize.y; y++)
		{
			//ustaw vertexy
			rVaLevel[aktualnyVertex + 0].position = Vector2f(x * TILE_SIZE, y * TILE_SIZE);

			rVaLevel[aktualnyVertex + 1].position = Vector2f((x * TILE_SIZE) + TILE_SIZE, y * TILE_SIZE);

			rVaLevel[aktualnyVertex + 2].position = Vector2f((x * TILE_SIZE) + TILE_SIZE, (y * TILE_SIZE) + TILE_SIZE);

			rVaLevel[aktualnyVertex + 3].position = Vector2f(x * TILE_SIZE, (y * TILE_SIZE) + TILE_SIZE);

			//wybor "tile'ow" ze sprite sheeta
			int verticalOffset = arrayLevel[y][x] * TILE_SIZE;

			rVaLevel[aktualnyVertex + 0].texCoords = Vector2f(0, 0 + verticalOffset);

			rVaLevel[aktualnyVertex + 1].texCoords = Vector2f(TILE_SIZE, 0 + verticalOffset);

			rVaLevel[aktualnyVertex + 2].texCoords = Vector2f(TILE_SIZE, TILE_SIZE + verticalOffset);

			rVaLevel[aktualnyVertex + 3].texCoords = Vector2f(0, TILE_SIZE + verticalOffset);

			aktualnyVertex = aktualnyVertex + VERTS_IN_QUAD;
		}
	}
	return arrayLevel;
}

Vector2i LevelManager::getLevelSize()
{
	return m_LevelSize;
}

int LevelManager::getCurrentLevel()
{
	return m_CurrentLevel;
}

Vector2f LevelManager::getStartPosition()
{
	return m_StartPosition;
}

void LevelManager::updateBlock(VertexArray& rVaLevel, int i, int j)
{
	int aktualnyVertex = 0;

	for (int x = 0; x < m_LevelSize.x; x++)
	{
		for (int y = 0; y < m_LevelSize.y; y++)
		{
			if (y == i && x == j)
			{
				//ustaw vertexy
				rVaLevel[aktualnyVertex + 0].position = Vector2f(x * TILE_SIZE, y * TILE_SIZE);

				rVaLevel[aktualnyVertex + 1].position = Vector2f((x * TILE_SIZE) + TILE_SIZE, y * TILE_SIZE);

				rVaLevel[aktualnyVertex + 2].position = Vector2f((x * TILE_SIZE) + TILE_SIZE, (y * TILE_SIZE) + TILE_SIZE);

				rVaLevel[aktualnyVertex + 3].position = Vector2f(x * TILE_SIZE, (y * TILE_SIZE) + TILE_SIZE);

				//wybor "tile'a" ktory ma zastapic pytajnik
				int verticalOffset = 3 * TILE_SIZE;

				rVaLevel[aktualnyVertex + 0].texCoords = Vector2f(0, 0 + verticalOffset);

				rVaLevel[aktualnyVertex + 1].texCoords = Vector2f(TILE_SIZE, 0 + verticalOffset);

				rVaLevel[aktualnyVertex + 2].texCoords = Vector2f(TILE_SIZE, TILE_SIZE + verticalOffset);

				rVaLevel[aktualnyVertex + 3].texCoords = Vector2f(0, TILE_SIZE + verticalOffset);
			}
			aktualnyVertex = aktualnyVertex + VERTS_IN_QUAD;
		}
	}
}

void LevelManager::setCurrentLevel(int i)
{
	m_CurrentLevel = i;
}