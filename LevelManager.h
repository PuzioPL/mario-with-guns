#pragma once
#include <SFML/Graphics.hpp>

using namespace sf;
using namespace std;

class LevelManager
{
private:
	Vector2i m_LevelSize;
	Vector2f m_StartPosition;
	int m_CurrentLevel = 0;
	const int NUM_LEVELS = 3;

public:
	const int TILE_SIZE = 50;
	const int VERTS_IN_QUAD = 4;

	Vector2f getStartPosition();
	int** nextLevel(VertexArray& rVaLevel);
	Vector2i getLevelSize();
	int getCurrentLevel();

	//ustaw poziom, do resetowania progressu
	void setCurrentLevel(int i);

	void updateBlock(VertexArray& rVaLevel,int j ,int i);
};