#include "stdafx.h"
#include "Engine.h"

void Engine::loadLevel()
{
	//usun wczesniej zaalocowana pamiec
	for (int i = 0; i < m_LevelManager.getLevelSize().y; i++)
	{
		delete[] m_ArrayLevel[i];
	}
	delete[] m_ArrayLevel;
	
	for (int i = 0; i < ENEMIES_NUM; i++)
	{
		m_Goombas[i].setAlive(false);
		m_Goombas[i].setActive(false);
	}

	m_ArrayLevel = m_LevelManager.nextLevel(m_VALevel);
	m_GameTimeLevelSec = 0;
	m_GameTimeLevelMin = 0;

	m_Mario.spawn(m_LevelManager.getStartPosition());
	m_Luigi.spawn(m_LevelManager.getStartPosition());

	int i = 0;
	int ilosc = 0;

	//switch ktory spawnuje przeciwnika jak znajdzie "9" w pliku z poziomem
	switch (m_LevelManager.getCurrentLevel())
	{
	case 1:
		for (int x = 0; x < m_LevelManager.getLevelSize().x; x++)
		{
			for (int y = 0; y < m_LevelManager.getLevelSize().y; y++)
			{
				if (m_ArrayLevel[y][x] == 9)
				{
					ilosc++;
					if(i < ilosc)
					{
						m_Goombas[i].spawn(Vector2f(x * TILE_SIZE, y * TILE_SIZE));						
					}
					i++;
				}
			}
		}
		break;
	case 2:
		for (int x = 0; x < m_LevelManager.getLevelSize().x; x++)
		{
			for (int y = 0; y < m_LevelManager.getLevelSize().y; y++)
			{
				if (m_ArrayLevel[y][x] == 9)
				{
					ilosc++;
					if (i < ilosc)
					{
						m_Goombas[i].spawn(Vector2f(x * TILE_SIZE, y * TILE_SIZE));
					}
					i++;
				}
			}
		}
		break;
	case 3:
		for (int x = 0; x < m_LevelManager.getLevelSize().x; x++)
		{
			for (int y = 0; y < m_LevelManager.getLevelSize().y; y++)
			{
				if (m_ArrayLevel[y][x] == 9)
				{
					ilosc++;
					if (i < ilosc)
					{
						m_Goombas[i].spawn(Vector2f(x * TILE_SIZE, y * TILE_SIZE));
					}
					i++;
				}
			}
		}
		break;
	}

	m_NewLevelRequired = false;
}