#include "stdafx.h"
#include "Luigi.h"
#include "TextureHolder.h"

Luigi::Luigi()
{
	m_Sprite = Sprite(TextureHolder::GetTexture("graphics/luigi_sheet.png"));
	m_Speed = 500;
	m_StalaSkoku = 1500;
	m_SzybkoscSkoku = 1800;
	m_MaxHealth = 80;
	m_CurrentHealth = m_MaxHealth;
}

//wirtualna funkcja
bool Luigi::handleInput()
{
	m_JustJumped = false;

	if (Keyboard::isKeyPressed(Keyboard::W))
	{
		//wykonaj skok o ile nie jest w trakcie skoku, ale tylko kiedy nie spada (stoi na klocku)
		if (!m_IsJumping && !m_IsFalling)
		{
			m_IsJumping = true;
			m_JustJumped = true;
		}
	}
	else
	{
		m_IsJumping = false;
		m_IsFalling = true;
	}

	if (Keyboard::isKeyPressed(Keyboard::A))
	{
		m_MoveLeft = true;
	}
	else
	{
		m_MoveLeft = false;
	}

	if (Keyboard::isKeyPressed(Keyboard::D))
	{
		m_MoveRight = true;
	}
	else
	{
		m_MoveRight = false;
	}

	//funkcja zwraca czy postac wykonala skok (true or false)
	return m_JustJumped;
}