#include "stdafx.h"
#include "Mario.h"
#include "TextureHolder.h"

Mario::Mario()
{
	m_Sprite = Sprite(TextureHolder::GetTexture("graphics/mario_sheet.png"));
	m_Speed = 400;
	m_StalaSkoku = 1800;
	m_SzybkoscSkoku = 1800;
	m_MaxHealth = 100;
	m_CurrentHealth = m_MaxHealth;
}

//wirtualna funkcja
bool Mario::handleInput()
{
	m_JustJumped = false;
	
	if (Keyboard::isKeyPressed(Keyboard::W))
	{
		//wykonaj skok o ile nie jest w trakcie skoku, ale tylko kiedy nie spada (stoi na klocku)
		if (!m_IsJumping && !m_IsFalling)
		{
			m_IsJumping = true;
			m_JustJumped = true;
		}
	}
	else
	{
		m_IsJumping = false;
		m_IsFalling = true;
	}
	
	if (Keyboard::isKeyPressed(Keyboard::A))
	{
		m_MoveLeft = true;
	}
	else
	{
		m_MoveLeft = false;
	}

	if (Keyboard::isKeyPressed(Keyboard::D))
	{
		m_MoveRight = true;
	}
	else
	{
		m_MoveRight = false;
	}
	
	//funkcja zwraca czy postac wykonala skok (true or false)
	return m_JustJumped;
}