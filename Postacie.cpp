#include "stdafx.h"
#include "Postacie.h"

void Postacie::spawn(Vector2f startPosition)
{
	m_Position.x = startPosition.x;
	m_Position.y = startPosition.y;

	m_Sprite.setPosition(m_Position);

	m_CurrentHealth = m_MaxHealth;
	m_source.x = 0;
	m_source.y = 0;
}

int Postacie::update(float elapsedTime, int isGunActive)
{
	//skok
	if (m_IsJumping)
	{
		if (m_JustJumped)
		{
			m_SzybkoscSkoku = m_StalaSkoku;
			m_JustJumped = false;
			m_IsFalling = false;
		}

		m_Position.y -= m_SzybkoscSkoku * elapsedTime;
			if (m_SzybkoscSkoku > 0)
			{
				m_SzybkoscSkoku -= 5000* elapsedTime;
			}
			else
			{
				m_IsJumping = false;
				m_IsFalling = true;
			}
		}
	
	if (m_IsFalling)
	{
		if (m_JustFell)
		{
			m_SzybkoscSkoku = 0.5;
			m_JustFell = false;
		}
		m_Position.y += m_SzybkoscSkoku * elapsedTime;
	if (m_SzybkoscSkoku < m_StalaSkoku)
		{
			m_SzybkoscSkoku += 5000 * elapsedTime;
		}
	}

	if (m_MoveRight)
	{
		m_Position.x += m_Speed * elapsedTime;
	}

	if (m_MoveLeft)
	{
		m_Position.x -= m_Speed * elapsedTime;
	}

	if (m_MoveLeft)
	{
		m_source.y = 0;
		m_FacingLeft = true;
	}
	if (m_MoveRight)
	{
		m_source.y = 1;
		m_FacingLeft = false;
	}

	
	if (m_IsJumping && m_MoveLeft) m_source.y = 2;
	if (m_IsJumping && m_MoveRight) m_source.y = 3;

	if (m_FacingLeft && !m_MoveLeft)
	{
		m_source.y = 0;
		m_source.x = 0;
	}
	else
		if (!m_FacingLeft && !m_MoveRight)
	{
		m_source.y = 1;
		m_source.x = 0;
	}

	m_Czas += elapsedTime;
	if (m_Czas > 0.25  && (m_MoveLeft || m_MoveRight))
	{
		m_source.x++;
		if (m_source.x * 50 >= 150)
		{
			m_source.x = 0;
		}
		m_Czas = 0;
	}

	if (isGunActive == 1 || isGunActive == 2)
	{
		m_source.y += 4;
	}
	
	m_Sprite.setTextureRect(sf::IntRect(m_source.x * 50, m_source.y * 96, 50, 96));

	//aktualizuj czesci hitboxa
	FloatRect r = getPosition();
	
	//stopy
	m_Stopy.left = r.left + 5;
	m_Stopy.top = r.top + r.height - 1;
	m_Stopy.width = r.width - 13;
	m_Stopy.height = 2;

	//glowa
	m_Glowa.left = r.left + 5;
	m_Glowa.top = r.top + 4;
	m_Glowa.width = r.width - 8;
	m_Glowa.height = 1;

	//prawo
	m_Prawo.left = r.left + r.width - 2;
	m_Prawo.top = r.top + 20;
	m_Prawo.width = 1;
	m_Prawo.height = r.height - 34;

	//lewo
	m_Lewo.left = r.left;
	m_Lewo.top = r.top + 20;
	m_Lewo.width = 1;
	m_Lewo.height = r.height - 34;

	m_Sprite.setPosition(m_Position);

	//*************************************************************************************************************
	//prostokaty ktore pokazuja hitboxy
	m_StopyShape.setSize(Vector2f(m_Stopy.width, m_Stopy.height));
	m_StopyShape.setPosition(Vector2f(m_Stopy.left, m_Stopy.top));
	m_StopyShape.setFillColor(Color::Red);

	m_GlowaShape.setSize(Vector2f(m_Glowa.width, m_Glowa.height));
	m_GlowaShape.setPosition(Vector2f(m_Glowa.left, m_Glowa.top));
	m_GlowaShape.setFillColor(Color::Green);

	m_PrawoShape.setSize(Vector2f(m_Prawo.width, m_Prawo.height));
	m_PrawoShape.setPosition(Vector2f(m_Prawo.left, m_Prawo.top));
	m_PrawoShape.setFillColor(Color::Yellow);

	m_LewoShape.setSize(Vector2f(m_Lewo.width, m_Lewo.height));
	m_LewoShape.setPosition(Vector2f(m_Lewo.left, m_Lewo.top));
	m_LewoShape.setFillColor(Color::Blue);

	return m_source.y;
}

FloatRect Postacie::getPosition()
{
	return m_Sprite.getGlobalBounds();
}

Vector2f Postacie::getCenter()
{
	return Vector2f(m_Position.x + m_Sprite.getGlobalBounds().width / 2, m_Position.y + m_Sprite.getGlobalBounds().height / 2);
}

FloatRect Postacie::getStopy()
{
	return m_Stopy;
}

FloatRect Postacie::getGlowa()
{
	return m_Glowa;
}

FloatRect Postacie::getPrawo()
{
	return m_Prawo;
}

FloatRect Postacie::getLewo()
{
	return m_Lewo;
}

Sprite Postacie::getSprite()
{
	return m_Sprite;
}

void Postacie::stopFall(float position)
{
	m_Position.y = position - getPosition().height;
	m_Sprite.setPosition(m_Position);
	m_IsFalling = false;
}

void Postacie::stopPrawo(float position)
{
	m_Position.x = position - m_Sprite.getGlobalBounds().width;
	m_Sprite.setPosition(m_Position);
}

void Postacie::stopLewo(float position)
{
	m_Position.x = position + m_Sprite.getGlobalBounds().width*1.1;
	m_Sprite.setPosition(m_Position);
}

void Postacie::stopJump()
{
	m_IsJumping = false;
	m_IsFalling = true;
}

//*************************************************************************************************************
//gettery na hitboxy
RectangleShape Postacie::getStopyShape()
{
	return m_StopyShape;
}

RectangleShape Postacie::getGlowaShape()
{
	return m_GlowaShape;
}

RectangleShape Postacie::getPrawoShape()
{
	return m_PrawoShape;
}

RectangleShape Postacie::getLewoShape()
{
	return m_LewoShape;
}

void Postacie::justFell(bool i)
{
	m_JustFell = i;
	if (m_JustFell == true)
	{
		m_Przysp = 8;
	}
}

void Postacie::isStanding(bool i)
{
	m_IsStanding = i;
}

bool Postacie::updateHealth(int i, float gameTimeTotal)
{
	if (gameTimeTotal - m_LastBeingHit > 0.5)
	{
		//kiedy i to -, postac otrzymala obrazenia
		//kiedy i to +, postac sie uleczyla
		m_CurrentHealth += i;
		m_LastBeingHit = gameTimeTotal;
		m_SM.playHit();
	}
	//kiedy po wyleczeniu postac ma wiecej zycia niz max
	if (m_CurrentHealth > m_MaxHealth)
	{
		m_CurrentHealth = m_MaxHealth;
	}	

	if (m_CurrentHealth < 1)
	{
		//postac umarla, zwroc true
		m_Sprite.setTextureRect(IntRect(0, 768, 50, 96));
		return true;
	}
	else
	{
		//postac zyje, zwroc false
		return false;
	}
}

int Postacie::getCurrentHealth()
{
	return m_CurrentHealth;
}