#pragma once
#include <SFML/Graphics.hpp>
#include "SoundManager.h"

using namespace sf;

class Postacie
{
protected:
	Sprite m_Sprite;

	bool m_IsJumping;
	bool m_IsFalling;
	bool m_IsStanding;
	float m_Czas;
	bool m_FacingLeft; // do okreslenia czy postac ma stac w lewa strone, czy w prawa

	Vector2i m_source; // do animacji sprite

	bool m_MoveLeft;
	bool m_MoveRight;
	float m_TimeThisJump;
	int m_CurrentHealth;
	int m_MaxHealth;
	float m_Speed;
	bool m_JustJumped = false; //do odtwarzania dzwieku
	bool m_JustFell;
	float m_Przysp;
	float m_StalaSkoku;
	float m_SzybkoscSkoku;

	//kiedy ostatnio zostal uderzony
	float m_LastBeingHit;

	//do odglosow postaci
	SoundManager m_SM;
private:
	Vector2f m_Position;

	FloatRect m_Stopy;
	FloatRect m_Glowa;
	FloatRect m_Prawo;
	FloatRect m_Lewo;

	RectangleShape m_StopyShape;
	RectangleShape m_GlowaShape;
	RectangleShape m_PrawoShape;
	RectangleShape m_LewoShape;

	Texture m_Texture;

public:
	void spawn(Vector2f startPosition);

	bool virtual handleInput() = 0; //czysta virtualna funkcja.
	//klasa jest abstrakcyjna i nie mozna z niej zrobic obiektu

	FloatRect getPosition();

	FloatRect getStopy();
	FloatRect getGlowa();
	FloatRect getPrawo();
	FloatRect getLewo();

	RectangleShape getStopyShape();
	RectangleShape getGlowaShape();
	RectangleShape getPrawoShape();
	RectangleShape getLewoShape();

	Sprite getSprite();

	void stopFall(float position);
	void stopPrawo(float position);
	void stopLewo(float position);
	void stopJump();

	Vector2f getCenter();

	int update(float dtAsSeconds, int isGunActive);

	void justFell(bool i);

	void isStanding(bool i);

	//do zbierania apteczek lub otrzymywania obrazen
	bool updateHealth(int i, float dtAsSeconds);

	int getCurrentHealth();
};