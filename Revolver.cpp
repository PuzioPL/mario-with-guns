#include "stdafx.h"
#include "Revolver.h"
#include "TextureHolder.h"

Revolver::Revolver()
{
	m_SpriteRevMario = Sprite(TextureHolder::GetTexture("graphics/revolver_mario.png"));
	m_SpriteRevMario.setOrigin(8, 15);

	m_SpriteRevLuigi = Sprite(TextureHolder::GetTexture("graphics/revolver_luigi.png"));
	m_SpriteRevLuigi.setOrigin(8, 15);
}

bool Revolver::handleInput()
{
	return m_JustShot; // do odtawrzania dzwieku
}