#include "stdafx.h"
#include "SoundManager.h"

using namespace sf;

SoundManager::SoundManager()
{
	//zaladuj dzwieki do bufferow i ustaw buffery do dzwiekow
	m_ShootBuffer.loadFromFile("sound/shoot.wav");
	m_ShootSound.setBuffer(m_ShootBuffer);

	m_HitBuffer.loadFromFile("sound/hit.wav");
	m_HitSound.setBuffer(m_HitBuffer);
}

void SoundManager::playShoot()
{
	m_ShootSound.setRelativeToListener(true);
	m_ShootSound.play();
}

void SoundManager::playHit()
{
	m_HitSound.setRelativeToListener(true);
	m_HitSound.play();
}