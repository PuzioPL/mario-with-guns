#pragma once
#include <SFML/Audio.hpp>

using namespace sf;

class SoundManager
{
private:
	SoundBuffer m_ShootBuffer;
	Sound m_ShootSound;

	SoundBuffer m_HitBuffer;
	Sound m_HitSound;

public:
	SoundManager();

	void playShoot();
	void playHit();
};