#include "stdafx.h"
#include "TextureHolder.h"
#include <assert.h>

using namespace sf;
using namespace std;
TextureHolder* TextureHolder::m_s_Instance = nullptr;

TextureHolder::TextureHolder()
{
	assert(m_s_Instance == nullptr);
	m_s_Instance = this;
}

Texture& TextureHolder::GetTexture(string const& filename)
{
	//wez referencje z m_Texture przez m_s_Instance
	map<string, Texture>& m = m_s_Instance->m_Textures;

	//iteracjakey-value-pair aby mozna bylo szukac kvp przy pomocy filename
	auto keyValuePair = m.find(filename);
	//auto  = "map<string, Texture>::iteration"

	if (keyValuePair != m.end())
	{
		//znaleziono. Zwroc druga czesc kvp, czyli teksture
		return keyValuePair->second;
	}
	else
	{
		//nie znaleziono. zaladuj texture z pliku "filename"
		auto& texture = m[filename];
		texture.loadFromFile(filename);

		return texture;
	}
}