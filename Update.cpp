#include "stdafx.h"
#include "Engine.h"
#include <SFML/Graphics.hpp>
#include <sstream>

using namespace sf;

void Engine::update(float dtAsSeconds)
{
	if (m_NewLevelRequired)
	{
		stringstream ssTime;
		stringstream ssScoreWin;

		//zapisuje czas ukonczenia danego poziomu do zmiennej
		switch (m_LevelManager.getCurrentLevel())
		{
		case 1:
			ssTime << "Level 1 time: " << (int)m_GameTimeLevelMin << "." << (int)m_GameTimeLevelSec << "s";
			m_Hud.setLevel1Time(ssTime.str());
			break;
		case 2:
			ssTime << "Level 2 time: " << (int)m_GameTimeLevelMin << "." << (int)m_GameTimeLevelSec << "s";
			m_Hud.setLevel2Time(ssTime.str());
			break;
		case 3:
			ssTime << "Level 3 time: " << (int)m_GameTimeLevelMin << "." << (int)m_GameTimeLevelSec << "s";
			m_Hud.setLevel3Time(ssTime.str());


			ssScoreWin << "Ending score: " << m_CurrentScore;
			m_Hud.setScoreWin(ssScoreWin.str());
		}

		if (m_LevelManager.getCurrentLevel() == 3)
		{
			m_State = State::WON;
			m_LevelManager.setCurrentLevel(0);
		}
		else
		{
			//laduje nowy poziom
			loadLevel();
			m_NewLevelRequired = false;
		}
		if (m_LevelManager.getCurrentLevel() == 1)
		{
			m_CurrentScore = 0;
		}
	}
	if (m_State == State::PLAYING)
	{
		m_GameTimeLevelSec += dtAsSeconds;

		m_MouseWorldPosition = m_Window.mapPixelToCoords(Mouse::getPosition(), m_MainView);

		if (m_MarioActive)
		{
			//update mario
			m_Facing = m_Mario.update(dtAsSeconds, m_IsGunActive);

			//wykrywanie kolizji
			if (detectCollisionsChar(m_Mario, dtAsSeconds))
			{
				//nowy poziom
			//	m_NewLevelRequired = true;

				//dzwiek konca poziomu
			}
			m_MainView.setCenter(m_Mario.getCenter().x, VideoMode::getDesktopMode().height / 2);
			m_BGMainView.setCenter((m_Mario.getCenter().x) / 4.5, VideoMode::getDesktopMode().height / 2);
		}
		else
		{
			//update luigi
			m_Facing = m_Luigi.update(dtAsSeconds, m_IsGunActive);
			if (detectCollisionsChar(m_Luigi, dtAsSeconds))
			{
				//m_NewLevelRequired = true;
				//dzwiek konca poziomu
			}
				m_MainView.setCenter(m_Luigi.getCenter().x, VideoMode::getDesktopMode().height / 2);
				m_BGMainView.setCenter((m_Luigi.getCenter().x) / 4.5, VideoMode::getDesktopMode().height / 2);
		}

		for (int i = 0; i < ENEMIES_NUM; i++)
		{
			if (m_Goombas[i].isAlive())
			{
				detectCollisionsEnemy(m_Goombas[i]);
			}
			//update przeciwnikow, zalenie od aktywnej postaci
			if (m_MarioActive)
			{
				m_Goombas[i].update(dtAsSeconds, m_Mario.getCenter());
			}
			else
			{
				m_Goombas[i].update(dtAsSeconds, m_Luigi.getCenter());
			}
		}
	
		//update pociski
		for (int i = 0; i < BULLETS_NUM; i++)
		{
			if (m_Bullets[i].isInFlight())
			{
				m_Bullets[i].update(dtAsSeconds);
				detectCollisionsBullet(m_Bullets[i]);
			}
		}
	}
	
	Vector2f Facing;
	
	if (m_IsGunActive == 1 || m_IsGunActive == 2)
	{
		if (m_MarioActive)
		{
			if (m_Facing == 0 || m_Facing == 2 || m_Facing == 4 || m_Facing == 6) // kiedy postac skierowana w lewo
			{
				Facing.x = m_Mario.getCenter().x + 8; // m_Mario.getCenter().x wynosi 25 (bo polowa sprite'u Mario). Dodaje 12, aby pistolet byl w odpowiedniej pozycji
				Facing.y = m_Mario.getCenter().y + 8; // m_Mario.getCenter().y wynosi 48 (bo polowa sprite'u Mario). Dodaje 20, aby pistolet byl w odpowiedniej pozycji
			}
			else if (m_Facing == 1 || m_Facing == 3 || m_Facing == 5 || m_Facing == 7) // kiedy postac skierowana w prawo
			{
				Facing.x = m_Mario.getCenter().x - 4;
				Facing.y = m_Mario.getCenter().y + 8;
			}
			m_Revolver.update(dtAsSeconds, m_MouseWorldPosition, Facing);
		}

		if (!m_MarioActive)
		{

			if (m_Facing == 0 || m_Facing == 2 || m_Facing == 4 || m_Facing == 6) // kiedy postac skierowana w lewo
			{
				Facing.x = m_Luigi.getCenter().x + 8; // podobnie jak z mario
				Facing.y = m_Luigi.getCenter().y + 5;
			}
			else if (m_Facing == 1 || m_Facing == 3 || m_Facing == 5 || m_Facing == 7) // kiedy postac skierowana w prawo
			{
				Facing.x = m_Luigi.getCenter().x - 4;
				Facing.y = m_Luigi.getCenter().y + 5;
			}
			m_Revolver.update(dtAsSeconds, m_MouseWorldPosition, Facing);
		}
		
	}

	//UPDATE HUD
	//czy update'owac hud?
	m_FramesSinceLastHUDUpdate++;

	//jezeli juz minelo m_TargetFramesPerHUDUpdate czasu, update hud
	if (m_FramesSinceLastHUDUpdate > m_TargetFramesPerHUDUpdate)
	{
		//update game HUD text
		stringstream ssTime;
		stringstream ssLevel;
		stringstream ssScore;
		stringstream ssHealth;		
		
		//update czas
		//kiedy minela minuta, dodaj minute i zeruj sekundy
		if ((int)m_GameTimeLevelSec % 60 == 0 && (int)m_GameTimeLevelSec !=0)
		{
			m_GameTimeLevelMin++;
			m_GameTimeLevelSec = 0;
		}
		
		ssTime << "Time: " << (int)m_GameTimeLevelMin << "." <<(int)m_GameTimeLevelSec << "s";
		m_Hud.setTime(ssTime.str());

		//update level
		ssLevel << "Level:" << m_LevelManager.getCurrentLevel();
		m_Hud.setLevel(ssLevel.str());

		//update score
		ssScore << "Score:" << m_CurrentScore;
		m_Hud.setScore(ssScore.str());

		if (m_MarioActive)
		{
			ssHealth << m_Mario.getCurrentHealth();
			m_Hud.setHealthBar(m_Mario.getCurrentHealth());
		}
		else
		{
			ssHealth << m_Luigi.getCurrentHealth();
			m_Hud.setHealthBar(m_Luigi.getCurrentHealth());
		}
		m_Hud.setHealth(ssHealth.str());

		//wyzeruj licznik
		m_FramesSinceLastHUDUpdate = 0;
	}
	//main view
	
}